FROM fedora:23
MAINTAINER Freddy GRANDIERE <freddy.grandiere@me.com>

# Install enhanced tools
RUN dnf -y install bash-completion unzip

# Install httpd
RUN dnf -y install httpd

# install php and php-modules
RUN dnf -y install php php-mysql

# Install mariadb server and mysql client
RUN dnf -y install mariadb-server mysql php-mysql